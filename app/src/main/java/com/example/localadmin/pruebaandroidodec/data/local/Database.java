package com.example.localadmin.pruebaandroidodec.data.local;

import android.content.ContentValues;
import android.database.Cursor;

import com.example.localadmin.pruebaandroidodec.data.model.Post;
import com.example.localadmin.pruebaandroidodec.data.model.User;

import java.util.ArrayList;
import java.util.List;

class Database {

    public abstract static class UsersTable {
        public static final String TABLE_NAME = "user";
        public static final String COLUMN_ID = "id";
        public static final String COLUMN_NAME = "name";
        public static final String COLUMN_USERNAME = "username";
        public static final String COLUMN_EMAIL = "email";

        public static final String CREATE = "CREATE TABLE "
                + TABLE_NAME + "("
                + COLUMN_ID + " INT PRIMARY KEY NOT NULL UNIQUE, "
                + COLUMN_NAME + " TEXT, "
                + COLUMN_USERNAME + " TEXT, "
                + COLUMN_EMAIL + " TEXT"
                + ")";

        public static ContentValues toContentValues(User user) {
            ContentValues values = new ContentValues();
            values.put(COLUMN_ID, user.getId());
            values.put(COLUMN_NAME, user.getName());
            values.put(COLUMN_USERNAME, user.getUsername());
            values.put(COLUMN_EMAIL, user.getEmail());

            return values;
        }

        public static List<User> parseCursor(Cursor cursor) {
            List<User> usersToReturn = new ArrayList<>();

            while (cursor.moveToNext()) {
                User user = new User();
                user.setId(cursor.getInt(cursor.getColumnIndexOrThrow(COLUMN_ID)));
                user.setName(cursor.getString(cursor.getColumnIndexOrThrow(COLUMN_NAME)));
                user.setUsername(cursor.getString(cursor.getColumnIndexOrThrow(COLUMN_USERNAME)));
                user.setEmail(cursor.getString(cursor.getColumnIndexOrThrow(COLUMN_EMAIL)));

                usersToReturn.add(user);
            }

            return usersToReturn;
        }
    }

    public abstract static class PostsTable {
        public static final String TABLE_NAME = "post";
        public static final String COLUMN_ID = "id";
        public static final String COLUMN_USER_ID = "userid";
        public static final String COLUMN_TITLE = "title";
        public static final String COLUMN_BODY = "body";

        public static final String CREATE = "CREATE TABLE "
                + TABLE_NAME + "("
                + COLUMN_ID + " INT PRIMARY KEY NOT NULL UNIQUE, "
                + COLUMN_USER_ID + " INT, "
                + COLUMN_TITLE + " TEXT, "
                + COLUMN_BODY + " TEXT, "
                + "FOREIGN KEY(" + COLUMN_USER_ID + ") REFERENCES " + UsersTable.TABLE_NAME + "(" + UsersTable.COLUMN_ID + ")"
                + ")";

        public static ContentValues toContentValues(Post post) {
            ContentValues values = new ContentValues();
            values.put(COLUMN_ID, post.getId());
            values.put(COLUMN_USER_ID, post.getUserId());
            values.put(COLUMN_TITLE, post.getTitle());
            values.put(COLUMN_BODY, post.getBody());

            return values;
        }

        public static List<Post> parseCursor(Cursor cursor) {
            List<Post> postsToReturn = new ArrayList<>();

            while(cursor.moveToNext()) {
                Post post = new Post();

                post.setId(cursor.getInt(cursor.getColumnIndexOrThrow(COLUMN_ID)));
                post.setUserId(cursor.getInt(cursor.getColumnIndexOrThrow(COLUMN_USER_ID)));
                post.setTitle(cursor.getString(cursor.getColumnIndexOrThrow(COLUMN_TITLE)));
                post.setBody(cursor.getString(cursor.getColumnIndexOrThrow(COLUMN_BODY)));

                postsToReturn.add(post);
            }

            return postsToReturn;
        }
    }
}
