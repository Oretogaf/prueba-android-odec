package com.example.localadmin.pruebaandroidodec.data.client;

import com.example.localadmin.pruebaandroidodec.data.model.User;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

public interface UserClient {
    @GET("users")
    Call<List<User>> getData();
}
