package com.example.localadmin.pruebaandroidodec.data.local;

import android.arch.persistence.db.SupportSQLiteDatabase;
import android.arch.persistence.db.SupportSQLiteOpenHelper.Callback;

class DbCallback extends Callback {
    private static final int VERSION = 1;

    DbCallback() {
        super(VERSION);
    }

    @Override
    public void onCreate(SupportSQLiteDatabase db) {
        db.execSQL(Database.UsersTable.CREATE);
        db.execSQL(Database.PostsTable.CREATE);
    }

    @Override
    public void onUpgrade(SupportSQLiteDatabase db, int oldVersion, int newVersion) {
    }
}
