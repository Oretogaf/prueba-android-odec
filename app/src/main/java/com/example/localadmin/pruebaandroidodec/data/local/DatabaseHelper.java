package com.example.localadmin.pruebaandroidodec.data.local;

import android.app.Application;
import android.arch.persistence.db.SupportSQLiteOpenHelper;
import android.arch.persistence.db.SupportSQLiteOpenHelper.Configuration;
import android.arch.persistence.db.framework.FrameworkSQLiteOpenHelperFactory;
import android.database.sqlite.SQLiteDatabase;

import com.example.localadmin.pruebaandroidodec.data.model.Post;
import com.example.localadmin.pruebaandroidodec.data.model.User;
import com.squareup.sqlbrite3.BriteDatabase;
import com.squareup.sqlbrite3.SqlBrite;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

public class DatabaseHelper {
    private final BriteDatabase mBD;

    public DatabaseHelper(Application application) {
        Configuration configuration = Configuration.builder(application)
                .name("userspost.db")
                .callback(new DbCallback())
                .build();

        SupportSQLiteOpenHelper.Factory factory = new FrameworkSQLiteOpenHelperFactory();
        SupportSQLiteOpenHelper helper = factory.create(configuration);
        mBD = (new SqlBrite.Builder()).build().wrapDatabaseHelper(helper, Schedulers.io());
    }

    public void setUsers(List<User> newUsers) {
        mBD.delete(Database.UsersTable.TABLE_NAME, null);
        for (User user : newUsers) {
            mBD.insert(Database.UsersTable.TABLE_NAME,
                    SQLiteDatabase.CONFLICT_REPLACE,
                    Database.UsersTable.toContentValues(user));
        }
    }

    public List<User> getUsers() {
        final List<User> usersFromDatabase = new ArrayList<>();

        Observable<SqlBrite.Query> users = mBD.createQuery(Database.UsersTable.TABLE_NAME,
                "SELECT * FROM " + Database.UsersTable.TABLE_NAME);
        users.subscribe(new Consumer<SqlBrite.Query>() {
            @Override
            public void accept(SqlBrite.Query query) throws Exception {
                usersFromDatabase.addAll(Database.UsersTable.parseCursor(query.run()));
            }
        });

        return usersFromDatabase;
    }

    public void setPosts(List<Post> newPosts) {
        mBD.delete(Database.PostsTable.TABLE_NAME, null);
        for (Post post : newPosts) {
            mBD.insert(Database.PostsTable.TABLE_NAME,
                    SQLiteDatabase.CONFLICT_REPLACE,
                    Database.PostsTable.toContentValues(post));
        }
    }

    public List<Post> getPosts(int userId) {
        final List<Post> postsFromDatabase = new ArrayList<>();

        Observable<SqlBrite.Query> users = mBD.createQuery(Database.PostsTable.TABLE_NAME,
                "SELECT * FROM " + Database.PostsTable.TABLE_NAME
                        + " WHERE " + Database.PostsTable.COLUMN_USER_ID + " = " + userId);
        users.subscribe(new Consumer<SqlBrite.Query>() {
            @Override
            public void accept(SqlBrite.Query query) throws Exception {
                postsFromDatabase.addAll(Database.PostsTable.parseCursor(query.run()));
            }
        });

        return postsFromDatabase;
    }
}
