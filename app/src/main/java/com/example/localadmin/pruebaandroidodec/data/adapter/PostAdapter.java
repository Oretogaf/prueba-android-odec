package com.example.localadmin.pruebaandroidodec.data.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.localadmin.pruebaandroidodec.R;
import com.example.localadmin.pruebaandroidodec.data.local.DatabaseHelper;
import com.example.localadmin.pruebaandroidodec.data.model.Post;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PostAdapter extends RecyclerView.Adapter<PostAdapter.PostViewHolder> {
    private final ArrayList<Post> mPosts;

    public PostAdapter(DatabaseHelper helper, int userId) {
        this.mPosts = (ArrayList<Post>) helper.getPosts(userId);
        this.notifyDataSetChanged();
    }

    @Override
    public PostAdapter.PostViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_post, parent, false);

        return new PostViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(PostAdapter.PostViewHolder holder, int position) {
        Post post = mPosts.get(position);
        holder.bindPost(post);
    }

    @Override
    public int getItemCount() {
        return mPosts.size();
    }

    public static class PostViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.item_post_tv_title) TextView title;
        @BindView(R.id.item_post_tv_body) TextView body;

        public PostViewHolder(View itemView) {
            super(itemView);

            ButterKnife.bind(this, itemView);
        }

        public void bindPost(Post post) {
            title.setText(post.getTitle());
            body.setText(post.getBody());
        }
    }
}
