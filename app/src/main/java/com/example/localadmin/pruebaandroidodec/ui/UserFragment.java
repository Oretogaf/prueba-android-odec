package com.example.localadmin.pruebaandroidodec.ui;

import android.app.Fragment;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.localadmin.pruebaandroidodec.MainActivity;
import com.example.localadmin.pruebaandroidodec.R;
import com.example.localadmin.pruebaandroidodec.data.adapter.UserAdapter;
import com.example.localadmin.pruebaandroidodec.data.model.User;

import butterknife.BindView;
import butterknife.ButterKnife;

public class UserFragment extends Fragment {
    @BindView(R.id.fragment_user_rv_users)
    RecyclerView recyclerViewUsers;
    private View mView;

    public UserFragment() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_user, container, false);
        return mView;
    }

    public  void onActivityCreated(Bundle state) {
        super.onActivityCreated(state);

        ButterKnife.bind(this, mView);
        init();
    }

    private void init() {
        final UserAdapter userAdapter;
        final MainActivity mainActivityInstance = (MainActivity) getActivity();

        recyclerViewUsers.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        recyclerViewUsers.setLayoutManager(layoutManager);
        userAdapter = new UserAdapter(MainActivity.helper);
        userAdapter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                User userSelected = (User) userAdapter.getItem(recyclerViewUsers.getChildAdapterPosition(view));
                mainActivityInstance.showPostFragment(userSelected.getId());
            }
        });
        recyclerViewUsers.setAdapter(userAdapter);
        recyclerViewUsers.addItemDecoration(new DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL));
    }
}
