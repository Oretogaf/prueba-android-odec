package com.example.localadmin.pruebaandroidodec.ui;

import android.app.Fragment;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.localadmin.pruebaandroidodec.MainActivity;
import com.example.localadmin.pruebaandroidodec.R;
import com.example.localadmin.pruebaandroidodec.data.adapter.PostAdapter;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PostFragment extends Fragment {
    @BindView(R.id.fragment_post_rv_posts)
    RecyclerView recyclerViewPosts;
    private int mUserId;
    private View mView;

    public PostFragment() {

    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_post, container, false);
        return mView;
    }

    public  void onActivityCreated(Bundle state) {
        super.onActivityCreated(state);

        Bundle bundle = getArguments();
        mUserId = bundle.getInt("userId");

        ButterKnife.bind(this, mView);
        init();
    }

    private void init() {
        recyclerViewPosts.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        recyclerViewPosts.setLayoutManager(layoutManager);
        PostAdapter postAdapter = new PostAdapter(MainActivity.helper, mUserId);
        recyclerViewPosts.setAdapter(postAdapter);
        recyclerViewPosts.addItemDecoration(new DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL));
    }
}
