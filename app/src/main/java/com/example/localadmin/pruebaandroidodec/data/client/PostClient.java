package com.example.localadmin.pruebaandroidodec.data.client;

import com.example.localadmin.pruebaandroidodec.data.model.Post;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

public interface PostClient {
    @GET("posts")
    Call<List<Post>> getData();
}
