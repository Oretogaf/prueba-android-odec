package com.example.localadmin.pruebaandroidodec;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;

import com.example.localadmin.pruebaandroidodec.data.client.PostClient;
import com.example.localadmin.pruebaandroidodec.data.client.UserClient;
import com.example.localadmin.pruebaandroidodec.data.local.DatabaseHelper;
import com.example.localadmin.pruebaandroidodec.data.model.Post;
import com.example.localadmin.pruebaandroidodec.data.model.User;
import com.example.localadmin.pruebaandroidodec.ui.PostFragment;
import com.example.localadmin.pruebaandroidodec.ui.UserFragment;
import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends FragmentActivity {
    public static DatabaseHelper helper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        helper = new DatabaseHelper(this.getApplication());
        loadJsonOfUsersByRest();

        showUserFragment();
    }

    private void loadJsonOfUsersByRest() {
        Gson gson = new GsonBuilder()
                .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
                .create();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://jsonplaceholder.typicode.com/")
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();
        UserClient userClient = retrofit.create(UserClient.class);
        Call<List<User>> userCall = userClient.getData();

        userCall.enqueue(new Callback<List<User>>() {
            @Override
            public void onResponse(Call<List<User>> call, Response<List<User>> response) {
                switch (response.code()) {
                    case 200:
                        ArrayList<User> users = (ArrayList<User>) response.body();
                        helper.setUsers(users);
                        loadJsonOfPostsByRest();
                        break;
                    default:
                        System.out.println("default");
                        break;
                }
            }

            @Override
            public void onFailure(Call<List<User>> call, Throwable t) {
                Log.e("error", t.toString());
            }
        });
    }

    private void loadJsonOfPostsByRest() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://jsonplaceholder.typicode.com/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        PostClient postClient = retrofit.create(PostClient.class);
        Call<List<Post>> postCall = postClient.getData();

        postCall.enqueue(new Callback<List<Post>>() {
            @Override
            public void onResponse(Call<List<Post>> call, Response<List<Post>> response) {
                switch (response.code()) {
                    case 200:
                        ArrayList<Post> posts = (ArrayList<Post>) response.body();
                        helper.setPosts(posts);
                        showUserFragment();
                        break;
                    default:
                        System.out.println("default");
                        break;
                }
            }

            @Override
            public void onFailure(Call<List<Post>> call, Throwable t) {
                Log.e("error", t.toString());
            }
        });
    }

    private void showUserFragment() {
        UserFragment userFragment = new UserFragment();

        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.addToBackStack(null);

        fragmentTransaction.add(R.id.activity_main_frag_fragment_container, userFragment);
        fragmentTransaction.commit();
    }

    public void showPostFragment(int userId) {
        PostFragment postFragment = new PostFragment();

        Bundle bundle = new Bundle();
        bundle.putInt("userId", userId);
        postFragment.setArguments(bundle);

        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.addToBackStack(null);

        fragmentTransaction.replace(R.id.activity_main_frag_fragment_container, postFragment);
        fragmentTransaction.commit();
    }
}
