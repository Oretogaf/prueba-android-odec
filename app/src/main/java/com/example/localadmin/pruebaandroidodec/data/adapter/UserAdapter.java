package com.example.localadmin.pruebaandroidodec.data.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.localadmin.pruebaandroidodec.R;
import com.example.localadmin.pruebaandroidodec.data.local.DatabaseHelper;
import com.example.localadmin.pruebaandroidodec.data.model.User;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class UserAdapter
        extends RecyclerView.Adapter<UserAdapter.UserViewHolder>
        implements View.OnClickListener {
    private final ArrayList<User> mUsers;
    private View.OnClickListener mListener;

    public UserAdapter(DatabaseHelper helper) {
        this.mUsers = (ArrayList<User>) helper.getUsers();
        this.notifyDataSetChanged();
    }

    @Override
    public UserViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_user, parent, false);
        itemView.setOnClickListener(this);

        return new UserViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(UserViewHolder holder, int position) {
        User user = mUsers.get(position);
        holder.bindUser(user);
    }

    @Override
    public int getItemCount() {
        return mUsers.size();
    }

    public Object getItem(int position) {
        return mUsers.get(position);
    }

    public static class UserViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.item_user_tv_username) TextView username;
        @BindView(R.id.item_user_tv_email) TextView email;

        public UserViewHolder(View itemView) {
            super(itemView);

            ButterKnife.bind(this, itemView);
        }

        public void bindUser(User user) {
            username.setText(user.getUsername());
            email.setText(user.getEmail());
        }
    }

    public void setOnClickListener(View.OnClickListener listener) {
        this.mListener = listener;
    }

    @Override
    public void onClick(View view) {
        if (mListener != null)
            mListener.onClick(view);
    }
}
